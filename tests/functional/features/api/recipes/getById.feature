Feature:
  As a user
  I want to get a recipe by identifier

  Background:
    Given the recipes from the fixtures have been loaded

  Scenario: It gets a recipe by id
    When I retrieve the recipe with id 1
    Then I am shown a success response
    And I am shown the recipe with id 1

  Scenario: It fails to get a recipe by unknown id
    When I retrieve the recipe with id 99
    Then I am shown a "not found" error with internal code 10400