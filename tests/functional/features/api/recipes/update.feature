Feature:
  As a user
  I want to update a recipe by identifier

  Background:
    Given the recipes from the fixtures have been loaded

  Scenario: It updates a recipe by id
    Given I have the following payload
    """
    {
      "general": {
        "title": "foo",
        "box_type": "vegetarian",
        "bullet_point_1": "foo"
      },
      "nutrition": {
        "calories_kcal": 99
      },
      "cuisine": {
        "season": "foo"
      }
    }
    """
    When I update the recipe with id 1
    Then I am shown a success response
    And the recipe 1 has been updated successfully

  Scenario: It fails to update a recipe with invalid box type
    Given I have the following payload
    """
    {
      "general": {
        "box_type": "foo"
      }
    }
    """
    When I update the recipe with id 2
    Then I am shown a "bad request" error with internal code 10000

  Scenario: It fails to update a recipe with invalid diet type
    Given I have the following payload
    """
    {
      "cuisine": {
        "diet_type": "foo"
      }
    }
    """
    When I update the recipe with id 3
    Then I am shown a "bad request" error with internal code 10000

  Scenario: It fails to update a recipe by unknown id
    When I update the recipe with id 69
    Then I am shown a "not found" error with internal code 10400