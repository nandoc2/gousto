Feature:
  As a user
  I want to retrieve all recipes with filters

  Background:
    Given the recipes from the fixtures have been loaded

  Scenario: It gets all recipes filter by cuisine
    When I retrieve the list of "asian" cuisine recipes
    Then I am shown a success response
    And I am shown the following recipes
      | id |
      | 1  |
      | 6  |

  Scenario: It gets all recipes filter by cuisine
    When I retrieve the list of "colombian" cuisine recipes
    Then I am shown a success response
    And I am shown the following recipes
      | id |
