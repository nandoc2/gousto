Feature:
  As a user
  I want to create a new recipe

  Scenario: It creates a recipe
    Given I have the following payload
    """
    {
      "gousto_reference": 10,
      "general": {
        "box_type": "gourmet",
        "title": "foo",
        "short_title": "foo",
        "slug": "foo",
        "marketing_description": "foo",
        "bullet_point_1": "foo",
        "bullet_point_2": "foo",
        "bullet_point_3": "foo"
      },
      "nutrition": {
        "calories_kcal": 1,
        "protein_grams": 1,
        "fat_grams": 1,
        "carbs_grams": 1
      },
      "cuisine": {
        "diet_type": "fish",
        "season": "foo",
        "base": "foo",
        "protein_source": "foo",
        "preparation_time_minutes": 1,
        "shelf_life_days": 1,
        "equipment_needed": "foo",
        "origin_country": "foo",
        "recipe_cuisine": "foo",
        "in_your_box": "foo"
      }
    }
    """
    When I create a recipe with the payload
    Then I am shown a success response
    And the recipe has been created successfully

  Scenario: It fails to create from missing attributes
    Given I have the following payload
    """
    {
      "gousto_reference": 10,
      "general": {
        "short_title": "foo",
        "slug": "foo",
        "marketing_description": "foo",
        "bullet_point_1": "foo",
        "bullet_point_2": "foo",
        "bullet_point_3": "foo"
      },
      "nutrition": {
        "calories_kcal": 1,
        "protein_grams": 1,
        "fat_grams": 1,
        "carbs_grams": 1
      }
    }
    """
    When I create a recipe with the payload
    # todo: app to handle this scenario with own exception
    Then I am shown a "server error" error with internal code 99999
