Feature:
  As a user
  I want to rate a recipe

  Background:
    Given the recipes from the fixtures have been loaded

  Scenario: It rates a recipe by id
    Given I have the following payload
    """
    {
      "star_rating": 4
    }
    """
    When I rate a recipe with id 1
    Then I am shown a success response
    And the recipe 1 has been rated successfully

  Scenario: It fails to rate a recipe with invalid star rating
    Given I have the following payload
    """
    {
      "star_rating": 90
    }
    """
    When I rate a recipe with id 1
    Then I am shown a "bad request" error with internal code 10000
