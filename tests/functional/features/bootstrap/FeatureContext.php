<?php

namespace Gousto\Tests\Functional\Features\Bootstrap;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Gousto\DataFixtures\AppFixtures;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * This context class contains the definitions of the steps used by the demo 
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 * 
 * @see http://behat.org/en/latest/quick_start.html
 */
class FeatureContext implements Context
{
    private const RESPONSE_TO_HTTP_CODE = [
        'bad request' => 400,
        'not found' => 404,
        'server error' => 500,
    ];

    /** @var KernelInterface */
    private $kernel;

    /** @var DBHandler */
    private $dbHandler;

    /** @var Response|null */
    private $response;

    /** @var array */
    private $payload = [];

    public function __construct(KernelInterface $kernel, DBHandler $dbHandler)
    {
        $this->kernel = $kernel;
        $this->dbHandler = $dbHandler;
    }

    /**
     * @Given the recipes from the fixtures have been loaded
     */
    public function theRecipesFromTheFixturesHaveBeenLoaded()
    {
        $container = $this->kernel->getContainer();
        $entityManager = $container->get('doctrine.orm.recipe_entity_manager');

        $loader = new Loader();
        $loader->addFixture(new AppFixtures());

        $purger = new ORMPurger();
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_DELETE);
        $executor = new ORMExecutor($entityManager, $purger);

        $executor->execute($loader->getFixtures());
    }

    /**
     * @When I retrieve the recipe with id :id
     *
     * @param int $id
     * @throws \Exception
     */
    public function iRetrieveTheRecipeWithId(int $id)
    {
        $this->response = $this->kernel->handle(Request::create('/api/recipes/' . $id, 'GET'));
    }

    /**
     * @Then /^I am shown a success response$/
     */
    public function iAmShownASuccessResponse()
    {
        Assert::assertSame(200, $this->response->getStatusCode());
    }

    /**
     * @Given I am shown the recipe with id :id
     *
     * @param int $id
     */
    public function iAmShownTheRecipeWithId(int $id)
    {
        $dbData = $this->dbHandler->selectFrom('recipe', 'id', (string) $id);
        $responseData = json_decode($this->response->getContent(), true)['data'];

        Assert::assertEquals($dbData['id'], $responseData['id']);
        Assert::assertEquals($dbData['gousto_reference'], $responseData['gousto_reference']);
    }

    /**
     * @Then I am shown a :error error with internal code :code
     *
     * @param string $error
     * @param int $code
     */
    public function iAmShownAErrorWithInternalCode(string $error, int $code)
    {
        $responseData = json_decode($this->response->getContent(), true)['error'];

        Assert::assertSame(self::RESPONSE_TO_HTTP_CODE[$error], $this->response->getStatusCode());
        Assert::assertSame($code, $responseData['code']);
    }

    /**
     * @Given I have the following payload
     *
     * @param PyStringNode $payload
     */
    public function iHaveTheFollowingPayload(PyStringNode $payload)
    {
        $this->payload = json_decode($payload->getRaw(), true);
    }

    /**
     * @When I update the recipe with id :id
     * @param int $id
     * @throws \Exception
     */
    public function iUpdateTheRecipeWithId(int $id)
    {
        $this->response = $this->kernel->handle(
            Request::create('/api/recipes/' . $id, 'PATCH', [], [], [], [], json_encode($this->payload))
        );
    }

    /**
     * @Given the recipe :id has been updated successfully
     * @param int $id
     */
    public function theRecipeHasBeenUpdatedSuccessfully(int $id)
    {
        $dbData = $this->dbHandler->selectFrom('recipe', 'id', (string) $id);
        $responseData = json_decode($this->response->getContent(), true)['data'];

        foreach ($this->payload as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $subKey => $subValue) {
                    Assert::assertEquals($subValue, $dbData[$subKey]);
                    Assert::assertEquals($subValue, $responseData[$key][$subKey]);
                }
            } else {
                Assert::assertEquals($value, $dbData[$key]);
                Assert::assertEquals($value, $responseData[$key]);
            }
        }
    }

    /**
     * @When I retrieve the list of :cuisine cuisine recipes
     */
    public function iRetrieveTheListOfCuisineRecipes(string $cuisine)
    {
        $this->response = $this->kernel->handle(Request::create('/api/recipes', 'GET', ['cuisine' => $cuisine]));
    }

    /**
     * @Given I am shown the following recipes
     * @param TableNode $expectedRecipes
     */
    public function iAmShownTheFollowingRecipes(TableNode $expectedRecipes)
    {
        $responseData = json_decode($this->response->getContent(), true)['data'];

        $responseRecipesIds = array_flip(array_map(function ($recipe) {
            return $recipe['id'];
        }, $responseData));

        $expectedNumberOfRecipes = 0;
        foreach ($expectedRecipes as $recipe) {
            Assert::assertArrayHasKey($recipe['id'], $responseRecipesIds);
            $expectedNumberOfRecipes ++;
        }

        Assert::assertSame($expectedNumberOfRecipes, count($responseRecipesIds));
    }

    /**
     * @When /^I create a recipe with the payload$/
     */
    public function iCreateARecipeWithThePayload()
    {
        $this->response = $this->kernel->handle(
            Request::create('/api/recipes', 'POST', [], [], [], [], json_encode($this->payload))
        );
    }

    /**
     * @Given the recipe has been created successfully
     */
    public function theRecipeHasBeenCreatedSuccessfully()
    {
        $responseData = json_decode($this->response->getContent(), true)['data'];

        Assert::assertNotEmpty($responseData['id']);

        foreach ($this->payload as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $subKey => $subValue) {
                    Assert::assertEquals($subValue, $responseData[$key][$subKey]);
                }
            } else {
                Assert::assertEquals($value, $responseData[$key]);
            }
        }
    }

    /**
     * @When I rate a recipe with id :id
     * @param int $id
     * @throws \Exception
     */
    public function iRateARecipeWithId(int $id)
    {
        $this->response = $this->kernel->handle(
            Request::create("/api/recipes/${id}/ratings", 'POST', [], [], [], [], json_encode($this->payload))
        );
    }

    /**
     * @Given the recipe :id has been rated successfully
     * @param int $id
     */
    public function theRecipeHasBeenRatedSuccessfully(int $id)
    {
        $responseData = json_decode($this->response->getContent(), true)['data'];

        Assert::assertEquals($id, $responseData['recipe']['id']);
        Assert::assertEquals($this->payload['star_rating'], $responseData['star_rating']);
    }
}
