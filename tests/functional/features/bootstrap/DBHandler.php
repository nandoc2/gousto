<?php
declare(strict_types=1);

namespace Gousto\Tests\Functional\Features\Bootstrap;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;

class DBHandler
{
    /** @var EntityManagerInterface */
    private $advertEntityManager;

    public function __construct(Registry $registry)
    {
        $this->advertEntityManager = $registry->getManager('recipe');
    }

    public function selectFrom(string $table, string $columnName, string $value): array
    {
        $conn = $this->advertEntityManager->getConnection();
        $stmt = $conn->executeQuery(sprintf('SELECT * FROM %s WHERE %s = ?', $table, $columnName), [$value]);
        return $stmt->fetch();
    }
}
