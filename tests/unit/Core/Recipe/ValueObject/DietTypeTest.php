<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\Recipe\ValueObject;

use Gousto\Core\Recipe\ValueObject\DietType;
use PHPUnit\Framework\TestCase;

class DietTypeTest extends TestCase
{
    /**
     * @test
     */
    public function itCreates(): void
    {
        $boxType = new DietType('fish');
        self::assertSame('fish', $boxType->getValue());
    }

    /**
     * @test
     */
    public function itThrowsFromInvalidValue(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new DietType('foo');
    }
}
