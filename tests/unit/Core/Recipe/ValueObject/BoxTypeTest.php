<?php
declare(strict_types=1);

namespace Gousto\Test\Core\Recipe\ValueObject;

use Gousto\Core\Recipe\ValueObject\BoxType;
use PHPUnit\Framework\TestCase;

class BoxTypeTest extends TestCase
{
    /**
     * @test
     */
    public function itCreates(): void
    {
        $boxType = new BoxType('gourmet');
        self::assertSame('gourmet', $boxType->getValue());
    }

    /**
     * @test
     */
    public function itThrowsFromInvalidValue(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new BoxType('foo');
    }
}
