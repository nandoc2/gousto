<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\Recipe;

use Gousto\Core\Recipe\RecipeBuilder;
use PHPUnit\Framework\TestCase;

class RecipeBuilderTest extends TestCase
{
    /**
     * @test
     * @throws \Exception
     */
    public function itCreatesRecipeFromArray(): void
    {
        $now = new \DateTimeImmutable();
        $data = [
            'gousto_reference' => RecipeFaker::GOUSTO_REFERENCE,
            'general' => [
                'box_type' => RecipeFaker::BOX_TYPE,
                'title' => RecipeFaker::TITLE,
                'short_title' => RecipeFaker::SHORT_TITLE,
                'slug' => RecipeFaker::SLUG,
                'marketing_description' => RecipeFaker::MARKETING_DESCRIPTION,
                'bullet_point_1' => RecipeFaker::BULLET_POINT_1,
                'bullet_point_2' => RecipeFaker::BULLET_POINT_2,
                'bullet_point_3' => RecipeFaker::BULLET_POINT_3,
            ],
            'nutrition' => [
                'calories_kcal' => RecipeFaker::CALORIES_KCAL,
                'protein_grams' => RecipeFaker::PROTEIN_GRAMS,
                'fat_grams' => RecipeFaker::FAT_GRAMS,
                'carbs_grams' => RecipeFaker::CARBS_GRAMS,
            ],
            'cuisine' => [
                'diet_type' => RecipeFaker::DIET_TYPE,
                'season' => RecipeFaker::SEASON,
                'base' => RecipeFaker::BASE,
                'protein_source' => RecipeFaker::PROTEIN_SOURCE,
                'preparation_time_minutes' => RecipeFaker::PREPARATION_TIME_MINUTES,
                'shelf_life_days' => RecipeFaker::SHELF_LIFE_DAYS,
                'equipment_needed' => RecipeFaker::EQUIPMENT_NEEDED,
                'origin_country' => RecipeFaker::ORIGIN_COUNTRY,
                'recipe_cuisine' => RecipeFaker::RECIPE_CUISINE,
                'in_your_box' => RecipeFaker::IN_YOUR_BOX,
            ],
        ];

        $recipe = RecipeBuilder::createRecipeFromArray($data, $now);

        self::assertNull($recipe->getId());
        self::assertSame($now, $recipe->getCreatedAt());
        self::assertSame($now, $recipe->getUpdatedAt());

        self::assertEquals(RecipeFaker::createRecipeGeneral(), $recipe->getGeneral());
        self::assertEquals(RecipeFaker::createRecipeNutrition(), $recipe->getNutrition());
        self::assertEquals(RecipeFaker::createRecipeCuisine(), $recipe->getCuisine());
    }

    /**
     * @test
     * @throws \Exception
     */
    public function itUpdatesRecipeFromArray(): void
    {
        $now = new \DateTimeImmutable();
        $data = [
            'general' => [
                'box_type' => 'gourmet',
                'title' => 'updated title',
            ],
            'nutrition' => [
                'calories_kcal' => 1234,
                'protein_grams' => 5678,
            ],
            'cuisine' => [
                'diet_type' => 'fish',
                'season' => 'updated season',
            ],
        ];

        $recipe = RecipeFaker::createRecipe();
        $updatedRecipe = RecipeBuilder::updateRecipeFromArray($recipe, $data, $now);

        self::assertSame($recipe->getId(), $updatedRecipe->getId());
        self::assertSame($recipe->getCreatedAt(), $updatedRecipe->getCreatedAt());
        self::assertSame($now, $updatedRecipe->getUpdatedAt());

        self::assertSame($data['general']['box_type'], $updatedRecipe->getGeneral()->getBoxType()->getValue());
        self::assertSame($data['general']['title'], $updatedRecipe->getGeneral()->getTitle());
        self::assertSame($data['nutrition']['calories_kcal'], $updatedRecipe->getNutrition()->getCaloriesKcal());
        self::assertSame($data['nutrition']['protein_grams'], $updatedRecipe->getNutrition()->getProteinGrams());
        self::assertSame($data['cuisine']['diet_type'], $updatedRecipe->getCuisine()->getDietType()->getValue());
        self::assertSame($data['cuisine']['season'], $updatedRecipe->getCuisine()->getSeason());
    }
}
