<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\Recipe\Dto;

use Gousto\Tests\Core\Recipe\RecipeFaker;
use PHPUnit\Framework\TestCase;

class RecipeCuisineTest extends TestCase
{
    /**
     * @test
     */
    public function itCreates(): void
    {
        $cuisine = RecipeFaker::createRecipeCuisine();

        self::assertSame(RecipeFaker::DIET_TYPE, $cuisine->getDietType()->getValue());
        self::assertSame(RecipeFaker::SEASON, $cuisine->getSeason());
        self::assertSame(RecipeFaker::BASE, $cuisine->getBase());
        self::assertSame(RecipeFaker::PROTEIN_SOURCE, $cuisine->getProteinSource());
        self::assertSame(RecipeFaker::PREPARATION_TIME_MINUTES, $cuisine->getPreparationTimeMinutes());
        self::assertSame(RecipeFaker::SHELF_LIFE_DAYS, $cuisine->getShelfLifeDays());
        self::assertSame(RecipeFaker::EQUIPMENT_NEEDED, $cuisine->getEquipmentNeeded());
        self::assertSame(RecipeFaker::ORIGIN_COUNTRY, $cuisine->getOriginCountry());
        self::assertSame(RecipeFaker::RECIPE_CUISINE, $cuisine->getRecipeCuisine());
        self::assertSame(RecipeFaker::IN_YOUR_BOX, $cuisine->getInYourBox());
    }
}
