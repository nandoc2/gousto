<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\Recipe\Dto;

use Gousto\Tests\Core\Recipe\RecipeFaker;
use PHPUnit\Framework\TestCase;

class RecipeNutritionTest extends TestCase
{
    /**
     * @test
     */
    public function itCreates(): void
    {
        $nutrition = RecipeFaker::createRecipeNutrition();

        self::assertSame(RecipeFaker::CALORIES_KCAL, $nutrition->getCaloriesKcal());
        self::assertSame(RecipeFaker::PROTEIN_GRAMS, $nutrition->getProteinGrams());
        self::assertSame(RecipeFaker::FAT_GRAMS, $nutrition->getFatGrams());
        self::assertSame(RecipeFaker::CARBS_GRAMS, $nutrition->getCarbsGrams());
    }
}
