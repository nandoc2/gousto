<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\Recipe\Dto;

use Gousto\Tests\Core\Recipe\RecipeFaker;
use PHPUnit\Framework\TestCase;

class RecipeGeneralTest extends TestCase
{
    /**
     * @test
     */
    public function itCreates(): void
    {
        $general = RecipeFaker::createRecipeGeneral();

        self::assertSame(RecipeFaker::BOX_TYPE, $general->getBoxType()->getValue());
        self::assertSame(RecipeFaker::TITLE, $general->getTitle());
        self::assertSame(RecipeFaker::SHORT_TITLE, $general->getShortTitle());
        self::assertSame(RecipeFaker::SLUG, $general->getSlug());
        self::assertSame(RecipeFaker::MARKETING_DESCRIPTION, $general->getMarketingDescription());
        self::assertSame(RecipeFaker::BULLET_POINT_1, $general->getBulletPoint1());
        self::assertSame(RecipeFaker::BULLET_POINT_2, $general->getBulletPoint2());
        self::assertSame(RecipeFaker::BULLET_POINT_3, $general->getBulletPoint3());
    }
}
