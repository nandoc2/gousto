<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\Recipe;

use PHPUnit\Framework\TestCase;

class RecipeTest extends TestCase
{
    /**
     * @test
     */
    public function itCreates(): void
    {
        $recipe = RecipeFaker::createRecipe();

        self::assertSame(RecipeFaker::ID, $recipe->getId());
        self::assertSame(RecipeFaker::GOUSTO_REFERENCE, $recipe->getGoustoReference());
        self::assertSame(RecipeFaker::CREATED_AT, $recipe->getCreatedAt()->format('Y-m-d H:i:s'));
        self::assertSame(RecipeFaker::UPDATED_AT, $recipe->getUpdatedAt()->format('Y-m-d H:i:s'));

        self::assertEquals(RecipeFaker::createRecipeGeneral(), $recipe->getGeneral());
        self::assertEquals(RecipeFaker::createRecipeNutrition(), $recipe->getNutrition());
        self::assertEquals(RecipeFaker::createRecipeCuisine(), $recipe->getCuisine());
    }
}
