<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\Recipe;

use Gousto\Core\Pagination\RequestPagination;
use Gousto\Core\Recipe\Recipe;
use Gousto\Core\Recipe\RecipeDbRepositoryInterface;
use Gousto\Core\Recipe\RecipeService;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class RecipeServiceTest extends TestCase
{
    /** @var RecipeDbRepositoryInterface|ObjectProphecy */
    private $dbRepository;

    /** @var RecipeService */
    private $service;

    public function setUp(): void
    {
        $this->dbRepository = $this->prophesize(RecipeDbRepositoryInterface::class);

        $this->service = new RecipeService($this->dbRepository->reveal());
    }

    /**
     * @test
     */
    public function itGetsById(): void
    {
        $this->dbRepository
            ->getById(1)
            ->shouldBeCalled();

        $this->service->getById(1);
    }

    /**
     * @test
     */
    public function itFindByCuisine(): void
    {
        $pagination = new RequestPagination(1, 1);

        $this->dbRepository
            ->findByCuisine($pagination, 'british')
            ->shouldBeCalled();

        $this->service->findByCuisine($pagination, 'british');
    }

    /**
     * @test
     */
    public function itUpdatesRecipe(): void
    {
        $recipe = RecipeFaker::createRecipe();

        $this->dbRepository
            ->updateRecipe($recipe)
            ->shouldBeCalled()
            ->willReturn(RecipeFaker::createRecipe());

        $updatedRecipe = $this->service->updateRecipe($recipe);

        self::assertInstanceOf(Recipe::class, $updatedRecipe);
    }

    /**
     * @test
     */
    public function itCreatesRecipe(): void
    {
        $recipe = RecipeFaker::createRecipe();

        $this->dbRepository
            ->persistRecipe($recipe)
            ->shouldBeCalled()
            ->willReturn(RecipeFaker::createRecipe());

        $persistedRecipe = $this->service->createRecipe($recipe);

        self::assertInstanceOf(Recipe::class, $persistedRecipe);
    }
}
