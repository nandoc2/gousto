<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\Recipe;

use DateTimeImmutable;
use Gousto\Core\Recipe\Dto\RecipeCuisine;
use Gousto\Core\Recipe\Dto\RecipeGeneral;
use Gousto\Core\Recipe\Dto\RecipeNutrition;
use Gousto\Core\Recipe\Recipe;
use Gousto\Core\Recipe\ValueObject\BoxType;
use Gousto\Core\Recipe\ValueObject\DietType;

class RecipeFaker
{
    public const ID = 12;
    public const GOUSTO_REFERENCE = 88;
    public const CREATED_AT = '2019-09-19 12:00:00';
    public const UPDATED_AT = '2019-10-10 12:00:00';

    public const DIET_TYPE = 'vegetarian';
    public const SEASON = 'foo';
    public const BASE = 'foo';
    public const PROTEIN_SOURCE = 'foo';
    public const PREPARATION_TIME_MINUTES = 1;
    public const SHELF_LIFE_DAYS = 60;
    public const EQUIPMENT_NEEDED = 'foo';
    public const ORIGIN_COUNTRY = 'uk';
    public const RECIPE_CUISINE = 'foo';
    public const IN_YOUR_BOX = 'foo';

    public const CALORIES_KCAL = 10;
    public const PROTEIN_GRAMS = 9;
    public const FAT_GRAMS = 22;
    public const CARBS_GRAMS = 10;

    public const BOX_TYPE = 'vegetarian';
    public const TITLE = 'foo';
    public const SHORT_TITLE = 'foo';
    public const SLUG = 'foo';
    public const MARKETING_DESCRIPTION = 'foo';
    public const BULLET_POINT_1 = 'foo';
    public const BULLET_POINT_2 = 'foo';
    public const BULLET_POINT_3 = 'foo';

    public static function createRecipe(): Recipe
    {
        return new Recipe(
            self::ID,
            self::GOUSTO_REFERENCE,
            new DateTimeImmutable(self::CREATED_AT),
            new DateTimeImmutable(self::UPDATED_AT),
            self::createRecipeGeneral(),
            self::createRecipeNutrition(),
            self::createRecipeCuisine()
        );
    }

    public static function createRecipeGeneral(): RecipeGeneral
    {
        return new RecipeGeneral(
            new BoxType(self::BOX_TYPE),
            self::TITLE,
            self::SHORT_TITLE,
            self::SLUG,
            self::MARKETING_DESCRIPTION,
            self::BULLET_POINT_1,
            self::BULLET_POINT_2,
            self::BULLET_POINT_3
        );
    }

    public static function createRecipeNutrition(): RecipeNutrition
    {
        return new RecipeNutrition(
            self::CALORIES_KCAL,
            self::PROTEIN_GRAMS,
            self::FAT_GRAMS,
            self::CARBS_GRAMS
        );
    }

    public static function createRecipeCuisine(): RecipeCuisine
    {
        return new RecipeCuisine(
            new DietType(self::DIET_TYPE),
            self::SEASON,
            self::BASE,
            self::PROTEIN_SOURCE,
            self::PREPARATION_TIME_MINUTES,
            self::SHELF_LIFE_DAYS,
            self::EQUIPMENT_NEEDED,
            self::ORIGIN_COUNTRY,
            self::RECIPE_CUISINE,
            self::IN_YOUR_BOX
        );
    }
}
