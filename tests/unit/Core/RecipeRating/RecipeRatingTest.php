<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\RecipeRating;

use Gousto\Core\RecipeRating\RecipeRating;
use Gousto\Core\RecipeRating\ValueObject\StarRating;
use Gousto\Tests\Core\Recipe\RecipeFaker;
use PHPUnit\Framework\TestCase;

class RecipeRatingTest extends TestCase
{
    /**
     * @test
     */
    public function itCreates(): void
    {
        $recipe = RecipeFaker::createRecipe();

        $recipeRating = new RecipeRating(
            1,
            $recipe,
            new StarRating(2)
        );

        self::assertSame(1, $recipeRating->getId());
        self::assertSame($recipe, $recipeRating->getRecipe());
        self::assertSame(2, $recipeRating->getStarRating()->getValue());
    }
}
