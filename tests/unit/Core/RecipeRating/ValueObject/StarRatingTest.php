<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\RecipeRating\ValueObject;

use Gousto\Core\RecipeRating\ValueObject\StarRating;
use PHPUnit\Framework\TestCase;

class StarRatingTest extends TestCase
{
    /**
     * @test
     */
    public function itCreates(): void
    {
        $starRating = new StarRating(3);
        self::assertSame(3, $starRating->getValue());
    }

    /**
     * @test
     */
    public function itThrowsFromInvalidValue(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new StarRating(99);
    }
}
