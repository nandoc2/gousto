<?php
declare(strict_types=1);

namespace Gousto\Tests\Core\RecipeRating;

use Gousto\Core\Recipe\RecipeService;
use Gousto\Core\RecipeRating\RecipeRating;
use Gousto\Core\RecipeRating\RecipeRatingDbRepositoryInterface;
use Gousto\Core\RecipeRating\RecipeRatingService;
use Gousto\Core\RecipeRating\ValueObject\StarRating;
use Gousto\Tests\Core\Recipe\RecipeFaker;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class RecipeRatingServiceTest extends TestCase
{
    /** @var RecipeRatingDbRepositoryInterface|ObjectProphecy */
    private $dbRepository;

    /** @var RecipeService|ObjectProphecy */
    private $recipeService;

    /** @var RecipeRatingService */
    private $service;

    public function setUp(): void
    {
        $this->dbRepository = $this->prophesize(RecipeRatingDbRepositoryInterface::class);
        $this->recipeService = $this->prophesize(RecipeService::class);

        $this->service = new RecipeRatingService(
            $this->dbRepository->reveal(),
            $this->recipeService->reveal()
        );
    }

    /**
     * @test
     */
    public function itRatesRecipe(): void
    {
        $recipeId = 1;
        $starRating = new StarRating(5);
        $recipe = RecipeFaker::createRecipe();

        $this->recipeService
            ->getById($recipeId)
            ->shouldBeCalled()
            ->willReturn($recipe);

        $this->dbRepository
            ->persistRecipeRating(Argument::that(function (RecipeRating $recipeRating) use ($recipe, $starRating) {
                self::assertNull($recipeRating->getId());
                self::assertSame($recipe, $recipeRating->getRecipe());
                self::assertSame($starRating, $recipeRating->getStarRating());
                return true;
            }))
            ->shouldBeCalled();

        $this->service->rateRecipe($recipeId, $starRating);
    }
}
