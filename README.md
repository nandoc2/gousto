# Gousto API

## Setup

```bash
> git clone git@bitbucket.org:nandoc2/gousto.git
> cd gousto
> cp .env.dist .env.local
> composer install
```

##### Prepare Database and load fixtures
Fixtures are loaded from CSV file in "fixtures/recipe-data.csv"

```bash
> bin/console doctrine:migrations:migrate
> bin/console doctrine:fixtures:load
```

##### Web Server
To make easier the setup you can run [PHP's built-in Web Server](https://symfony.com/doc/current/setup/built_in_web_server.html)
```bash
> php bin/console server:start
```

## Documentation

The API documentation can be found in

> localhost:8000/api/doc

All endpoints are documented and provides sample data

## Tests

Unit tests
```bash
> vendor/bin/phpunit
```

Functional tests
```bash
# Prepare Database
> bin/console doctrine:migrations:migrate --env=test

# Run tests
> vendor/bin/behat
```

## About this Test

> Your reasons for your choice of web application framework

Symfony is reliable, robust and recognized internationally, with a massive community.
Among other advantages:

- Faster development
- Maintainability
- Flexibility
- Long term support
- Good documentation

> How your solution would cater for different API consumers that require different recipe data 
e.g. a mobile app and the front-end of a website

- List endpoints are paginated, providing a subset of the data
- The endpoints also provide a resource expansion mechanism through "include" query string, helpful to offer granular information and reduce data return size

Among others, this projects follows SOLID principles, FP principles as pure functions and immutability.
