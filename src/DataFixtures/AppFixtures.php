<?php

namespace Gousto\DataFixtures;

use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\ORM\Mapping\ClassMetadata;
use Gousto\Core\Recipe\Dto\RecipeCuisine;
use Gousto\Core\Recipe\Dto\RecipeGeneral;
use Gousto\Core\Recipe\Dto\RecipeNutrition;
use Gousto\Core\Recipe\Recipe;
use Gousto\Core\Recipe\ValueObject\BoxType;
use Gousto\Core\Recipe\ValueObject\DietType;

class AppFixtures extends Fixture
{
    private const FIXTURE_FILE = __DIR__ . '/../../fixtures/recipe-data.csv';

    public function load(ObjectManager $manager)
    {
        if (($handle = fopen(self::FIXTURE_FILE, "r")) !== false) {
            // allow assign id
            $metadata = $manager->getClassMetadata(Recipe::class);
            $metadata->setIdGenerator(new AssignedGenerator());
            $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);

            $header = null;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($header === null) {
                    $header = $data;
                    continue;
                }

                $recipe = self::createRecipeFromArray(array_combine($header, $data));
                $manager->persist($recipe);
             }
        }

        $manager->flush();
    }

    private static function createRecipeFromArray(array $data): Recipe
    {
        return new Recipe(
            (int) $data['id'],
            (int) $data['gousto_reference'],
            DateTimeImmutable::createFromFormat('d/m/Y H:i:s', $data['created_at']),
            DateTimeImmutable::createFromFormat('d/m/Y H:i:s', $data['updated_at']),
            self::createRecipeGeneralFromArray($data),
            self::createRecipeNutritionFromArray($data),
            self::createRecipeCuisineFromArray($data)
        );
    }

    private static function createRecipeCuisineFromArray(array $data): RecipeCuisine
    {
        return new RecipeCuisine(
            new DietType($data['recipe_diet_type_id']),
            $data['season'],
            $data['base'] ?: null,
            $data['protein_source'],
            (int) $data['preparation_time_minutes'],
            (int) $data['shelf_life_days'],
            $data['equipment_needed'],
            $data['origin_country'],
            $data['recipe_cuisine'],
            $data['in_your_box'] ?: null
        );
    }

    private static function createRecipeGeneralFromArray(array $data): RecipeGeneral
    {
        return new RecipeGeneral(
            new BoxType($data['box_type']),
            $data['title'],
            $data['short_title'] ?: null,
            $data['slug'],
            $data['marketing_description'],
            $data['bulletpoint1'] ?: null,
            $data['bulletpoint2'] ?: null,
            $data['bulletpoint3'] ?: null
        );
    }

    private static function createRecipeNutritionFromArray(array $data): RecipeNutrition
    {
        return new RecipeNutrition(
            (int) $data['calories_kcal'],
            (int) $data['protein_grams'],
            (int) $data['fat_grams'],
            (int) $data['carbs_grams']
        );
    }
}
