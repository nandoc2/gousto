<?php
declare(strict_types=1);

namespace Gousto\Controller;

use Gousto\Core\Pagination\ResponsePagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ResponseEnvelope
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function renderSingular($object, array $context): JsonResponse
    {
        return new JsonResponse(
            $this->serializer->serialize(['data' => $object], 'json', $context),
            Response::HTTP_OK, [],
            true
        );
    }

    public function renderPlural(ResponsePagination $responsePagination, array $context): JsonResponse
    {
        $data = [
            'data' => $responsePagination->getItems(),
            // todo: add links
            'links' => [],
            'meta' => [
                'total' => $responsePagination->getTotal(),
                'count' => $responsePagination->getCount(),
                'page_size' => $responsePagination->getPageSize(),
                'current_page' => $responsePagination->getPageNumber()
            ]
        ];

        return new JsonResponse(
            $this->serializer->serialize($data, 'json', $context),
            Response::HTTP_OK, [],
            true
        );
    }
}
