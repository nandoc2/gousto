<?php
declare(strict_types=1);

namespace Gousto\Controller;

use Gousto\Core\RecipeRating\RecipeRatingService;
use Gousto\Core\RecipeRating\ValueObject\StarRating;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RecipeRatingController extends AbstractController
{
    /** @var RecipeRatingService */
    private $recipeRatingService;

    /** @var ResponseEnvelope */
    private $responseEnvelope;

    public function __construct(RecipeRatingService $recipeRatingService, ResponseEnvelope $responseEnvelope)
    {
        $this->recipeRatingService = $recipeRatingService;
        $this->responseEnvelope = $responseEnvelope;
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Recipe rated",
     *     @SWG\Schema(
     *       type="object"
     *     )
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="The recipe identifier",
     *     required=true
     * )
     * @SWG\Parameter(
     *     name="payload",
     *     in="body",
     *     type="string",
     *     description="Rating payload",
     *     required=true,
     *     @SWG\Schema(
     *       type="object",
     *       example={"star_rating": 4}
     *     )
     * )
     * @SWG\Tag(name="Recipes")
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function rateRecipe(int $id, Request $request): Response
    {
        $content = json_decode($request->getContent(), true);

        return $this->responseEnvelope->renderSingular(
            $this->recipeRatingService->rateRecipe($id, new StarRating($content['star_rating'])),
            []
        );
    }
}
