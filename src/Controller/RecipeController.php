<?php
declare(strict_types=1);

namespace Gousto\Controller;

use DateTimeImmutable;
use Gousto\Core\Pagination\RequestPagination;
use Gousto\Core\Recipe\RecipeBuilder;
use Gousto\Core\Recipe\RecipeService;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RecipeController extends AbstractController
{
    /** @var RecipeService */
    private $recipeService;

    /** @var ResponseEnvelope */
    private $responseEnvelope;

    public function __construct(RecipeService $recipeService, ResponseEnvelope $responseEnvelope)
    {
        $this->recipeService = $recipeService;
        $this->responseEnvelope = $responseEnvelope;
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns a recipe",
     *     @SWG\Schema(
     *       type="object"
     *     )
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="The recipe identifier",
     *     required=true
     * )
     * @SWG\Parameter(
     *     name="include",
     *     in="query",
     *     type="string",
     *     description="Comma separated list of resources to include",
     *     default="general,nutrition,cuisine"
     * )
     * @SWG\Tag(name="Recipes")
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function getById(int $id, Request $request): Response
    {
        $resources = explode(',', $request->get('include', ''));

        return $this->responseEnvelope->renderSingular($this->recipeService->getById($id), $resources);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns list of recipes",
     *     @SWG\Schema(
     *       type="object"
     *     )
     * )
     * @SWG\Parameter(
     *     name="cuisine",
     *     in="query",
     *     type="string",
     *     description="Filter recipes by cuisine",
     *     required=true,
     *     default="british"
     * )
     * @SWG\Parameter(
     *     name="include",
     *     in="query",
     *     type="string",
     *     description="Comma separated list of resources to include",
     *     default="general,nutrition,cuisine"
     * )
     * @SWG\Parameter(
     *     name="page_number",
     *     in="query",
     *     type="integer",
     *     description="Page number",
     *     default="1"
     * )
     * @SWG\Parameter(
     *     name="page_size",
     *     in="query",
     *     type="integer",
     *     description="Page size",
     *     default="4"
     * )
     * @SWG\Tag(name="Recipes")
     *
     * @param Request $request
     * @return Response
     */
    public function findByCuisine(Request $request): Response
    {
        $pagination = new RequestPagination(
            (int) $request->get('page_number', 1),
            (int) $request->get('page_size', 2)
        );

        $resources = explode(',', $request->get('include', ''));
        $cuisine = $request->get('cuisine', null);

        return $this->responseEnvelope->renderPlural(
            $this->recipeService->findByCuisine($pagination, $cuisine),
            $resources
        );
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Recipe updated",
     *     @SWG\Schema(
     *       type="object"
     *     )
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="The recipe identifier",
     *     required=true
     * )
     * @SWG\Parameter(
     *     name="payload",
     *     in="body",
     *     type="string",
     *     required=true,
     *     description="Recipe payload",
     *     @SWG\Schema(
     *       type="object",
     *       example={"general": {"title": "foo", "box_type": "vegetarian"}, "nutrition": {"calories_kcal": 10}}
     *     )
     * )
     * @SWG\Tag(name="Recipes")
     *
     * @param int $id
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function updateRecipe(int $id, Request $request): Response
    {
        $content = json_decode($request->getContent(), true);
        $recipe = $this->recipeService->getById($id);

        return $this->responseEnvelope->renderSingular(
            $this->recipeService->updateRecipe(
                RecipeBuilder::updateRecipeFromArray($recipe, $content, new DateTimeImmutable())
            ),
            ['general', 'nutrition', 'cuisine']
        );
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Recipe created",
     *     @SWG\Schema(
     *       type="object"
     *     )
     * )
     * @SWG\Parameter(
     *     name="payload",
     *     in="body",
     *     type="string",
     *     description="Recipe payload",
     *     required=true,
     *     @SWG\Schema(
     *       type="object",
     *       example={
     *         "gousto_reference": 10,
     *         "general": {
     *           "box_type": "gourmet",
     *           "title": "foo",
     *           "short_title": "foo",
     *           "slug": "foo",
     *           "marketing_description": "foo",
     *           "bullet_point_1": "foo",
     *           "bullet_point_2": "foo",
     *           "bullet_point_3": "foo"
     *         },
     *         "nutrition": {
     *           "calories_kcal": 1,
     *           "protein_grams": 1,
     *           "fat_grams": 1,
     *           "carbs_grams": 1
     *         },
     *         "cuisine": {
     *           "diet_type": "fish",
     *           "season": "foo",
     *           "base": "foo",
     *           "protein_source": "foo",
     *           "preparation_time_minutes": 1,
     *           "shelf_life_days": 1,
     *           "equipment_needed": "foo",
     *           "origin_country": "foo",
     *           "recipe_cuisine": "foo",
     *           "in_your_box": "foo"
     *         }
     *       }
     *     )
     * )
     * @SWG\Tag(name="Recipes")
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function createRecipe(Request $request): Response
    {
        $content = json_decode($request->getContent(), true);

        return $this->responseEnvelope->renderSingular(
            $this->recipeService->createRecipe(
                RecipeBuilder::createRecipeFromArray($content, new DateTimeImmutable())
            ),
            ['general', 'nutrition', 'cuisine']
        );
    }
}
