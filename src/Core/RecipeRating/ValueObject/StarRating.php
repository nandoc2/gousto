<?php
declare(strict_types=1);

namespace Gousto\Core\RecipeRating\ValueObject;

class StarRating
{
    private $value;

    public function __construct(int $starRating)
    {
        if ($starRating < 1 || $starRating > 5) {
            throw new \InvalidArgumentException("Invalid star rating value ${starRating}");
        }

        $this->value = $starRating;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
