<?php
declare(strict_types=1);

namespace Gousto\Core\RecipeRating;

interface RecipeRatingDbRepositoryInterface
{
    public function persistRecipeRating(RecipeRating $recipeRating): RecipeRating;
}
