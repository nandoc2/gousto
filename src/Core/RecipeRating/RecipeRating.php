<?php
declare(strict_types=1);

namespace Gousto\Core\RecipeRating;

use Gousto\Core\Recipe\Recipe;
use Gousto\Core\RecipeRating\ValueObject\StarRating;

class RecipeRating
{
    /** @var int|null */
    private $id;

    /** @var Recipe */
    private $recipe;

    /** @var StarRating */
    private $starRating;

    public function __construct(?int $id, Recipe $recipe, StarRating $starRating)
    {
        $this->id = $id;
        $this->recipe = $recipe;
        $this->starRating = $starRating;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecipe(): Recipe
    {
        return $this->recipe;
    }

    public function getStarRating(): StarRating
    {
        return $this->starRating;
    }
}
