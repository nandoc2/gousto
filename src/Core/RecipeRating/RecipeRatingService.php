<?php
declare(strict_types=1);

namespace Gousto\Core\RecipeRating;

use Gousto\Core\Recipe\RecipeService;
use Gousto\Core\RecipeRating\ValueObject\StarRating;

class RecipeRatingService
{
    /** @var RecipeRatingDbRepositoryInterface */
    private $dbRepository;

    /** @var RecipeService */
    private $recipeService;

    public function __construct(RecipeRatingDbRepositoryInterface $dbRepository, RecipeService $recipeService)
    {
        $this->dbRepository = $dbRepository;
        $this->recipeService = $recipeService;
    }

    public function rateRecipe(int $recipeId, StarRating $starRating): RecipeRating
    {
        return $this->dbRepository->persistRecipeRating(
            new RecipeRating(
                null,
                $this->recipeService->getById($recipeId),
                $starRating
            )
        );
    }
}
