<?php
declare(strict_types=1);

namespace Gousto\Core\Pagination;

class ResponsePagination extends RequestPagination
{
    /** @var int */
    private $total;

    /** @var mixed[] */
    private $items;

    public function __construct(int $pageNumber, int $pageSize, int $total, array $items)
    {
        parent::__construct($pageNumber, $pageSize);
        $this->total = $total;
        $this->items = $items;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return mixed[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getCount(): int
    {
        return count($this->items);
    }
}
