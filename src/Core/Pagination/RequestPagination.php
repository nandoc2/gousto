<?php
declare(strict_types=1);

namespace Gousto\Core\Pagination;

class RequestPagination
{
    /** @var int */
    private $pageNumber;

    /** @var int */
    private $pageSize;

    /**
     * @param int $pageNumber
     * @param int $pageSize
     */
    public function __construct(int $pageNumber, int $pageSize)
    {
        $this->pageNumber = $pageNumber;
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    public function calculateOffset(): int
    {
        return ($this->pageNumber - 1) * $this->pageSize;
    }
}
