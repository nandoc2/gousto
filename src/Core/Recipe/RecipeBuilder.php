<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe;

use DateTimeImmutable;
use Gousto\Core\Recipe\Dto\RecipeCuisine;
use Gousto\Core\Recipe\Dto\RecipeGeneral;
use Gousto\Core\Recipe\Dto\RecipeNutrition;
use Gousto\Core\Recipe\ValueObject\BoxType;
use Gousto\Core\Recipe\ValueObject\DietType;

class RecipeBuilder
{
    public static function createRecipeFromArray(array $data, DateTimeImmutable $now): Recipe
    {
        return new Recipe(
            null,
            $data['gousto_reference'],
            $now,
            $now,
            self::createRecipeGeneralFromArray($data['general']),
            self::createRecipeNutritionFromArray($data['nutrition']),
            self::createRecipeCuisineFromArray($data['cuisine'])
        );
    }

    public static function updateRecipeFromArray(Recipe $recipe, array $data, DateTimeImmutable $now): Recipe
    {
        return new Recipe(
            $recipe->getId(),
            $recipe->getGoustoReference(),
            $recipe->getCreatedAt(),
            $now,
            self::updateRecipeGeneralFromArray($recipe->getGeneral(), $data['general'] ?? null),
            self::updateRecipeNutritionFromArray($recipe->getNutrition(), $data['nutrition'] ?? null),
            self::updateRecipeCuisineFromArray($recipe->getCuisine(), $data['cuisine'] ?? null)
        );
    }

    private static function createRecipeCuisineFromArray(array $data): RecipeCuisine
    {
        return new RecipeCuisine(
            new DietType($data['diet_type']),
            $data['season'],
            $data['base'] ?: null,
            $data['protein_source'],
            (int) $data['preparation_time_minutes'],
            (int) $data['shelf_life_days'],
            $data['equipment_needed'],
            $data['origin_country'],
            $data['recipe_cuisine'],
            $data['in_your_box'] ?: null
        );
    }

    private static function updateRecipeCuisineFromArray(RecipeCuisine $recipeCuisine, ?array $data): RecipeCuisine
    {
        return $data === null
            ? $recipeCuisine
            : new RecipeCuisine(
                empty($data['diet_type'])
                    ? $recipeCuisine->getDietType()
                    : new DietType($data['diet_type']),
                $data['season'] ?? $recipeCuisine->getSeason(),
                $data['base'] ?? $recipeCuisine->getBase(),
                $data['protein_source'] ?? $recipeCuisine->getProteinSource(),
                isset($data['preparation_time_minutes']) && is_int($data['preparation_time_minutes'])
                    ? (int) $data['preparation_time_minutes']
                    : $recipeCuisine->getPreparationTimeMinutes(),
                isset($data['shelf_life_days']) && is_int($data['shelf_life_days'])
                    ? (int) $data['shelf_life_days']
                    : $recipeCuisine->getShelfLifeDays(),
                $data['equipment_needed'] ?? $recipeCuisine->getEquipmentNeeded(),
                $data['origin_country'] ?? $recipeCuisine->getOriginCountry(),
                $data['recipe_cuisine'] ?? $recipeCuisine->getRecipeCuisine(),
                $data['in_your_box'] ?? $recipeCuisine->getInYourBox()
            );
    }

    private static function createRecipeGeneralFromArray(array $data): RecipeGeneral
    {
        return new RecipeGeneral(
            new BoxType($data['box_type']),
            $data['title'],
            $data['short_title'] ?: null,
            $data['slug'],
            $data['marketing_description'],
            $data['bullet_point_1'] ?: null,
            $data['bullet_point_2'] ?: null,
            $data['bullet_point_3'] ?: null
        );
    }

    private static function updateRecipeGeneralFromArray(RecipeGeneral $recipeGeneral, ?array $data): RecipeGeneral
    {
        return $data === null
            ? $recipeGeneral
            : new RecipeGeneral(
                empty($data['box_type'])
                    ? $recipeGeneral->getBoxType()
                    : new BoxType($data['box_type']),
                $data['title'] ?? $recipeGeneral->getTitle(),
                $data['short_title'] ?? $recipeGeneral->getShortTitle(),
                $data['slug'] ?? $recipeGeneral->getSlug(),
                $data['marketing_description'] ?? $recipeGeneral->getMarketingDescription(),
                $data['bullet_point_1'] ?? $recipeGeneral->getBulletPoint1(),
                $data['bullet_point_2'] ?? $recipeGeneral->getBulletPoint2(),
                $data['bullet_point_3'] ?? $recipeGeneral->getBulletPoint3()
            );
    }

    private static function createRecipeNutritionFromArray(array $data): RecipeNutrition
    {
        return new RecipeNutrition(
            (int) $data['calories_kcal'],
            (int) $data['protein_grams'],
            (int) $data['fat_grams'],
            (int) $data['carbs_grams']
        );
    }

    private static function updateRecipeNutritionFromArray(RecipeNutrition $recipeNutrition, ?array $data): RecipeNutrition
    {
        return $data === null
            ? $recipeNutrition
            : new RecipeNutrition(
                isset($data['calories_kcal']) && is_int($data['calories_kcal'])
                    ? (int) $data['calories_kcal']
                    : $recipeNutrition->getCaloriesKcal(),
                isset($data['protein_grams']) && is_int($data['protein_grams'])
                    ? (int) $data['protein_grams']
                    : $recipeNutrition->getProteinGrams(),
                isset($data['fat_grams']) && is_int($data['fat_grams'])
                    ? (int) $data['fat_grams']
                    : $recipeNutrition->getFatGrams(),
                isset($data['carbs_grams']) && is_int($data['carbs_grams'])
                    ? (int) $data['carbs_grams']
                    : $recipeNutrition->getCarbsGrams()
            );
    }
}
