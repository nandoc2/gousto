<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe\Dto;

class RecipeNutrition
{
    /** @var int */
    private $caloriesKcal;

    /** @var int */
    private $proteinGrams;

    /** @var int */
    private $fatGrams;

    /** @var int */
    private $carbsGrams;

    public function __construct(int $caloriesKcal, int $proteinGrams, int $fatGrams, int $carbsGrams)
    {
        $this->caloriesKcal = $caloriesKcal;
        $this->proteinGrams = $proteinGrams;
        $this->fatGrams = $fatGrams;
        $this->carbsGrams = $carbsGrams;
    }

    public function getCaloriesKcal(): int
    {
        return $this->caloriesKcal;
    }

    public function getProteinGrams(): int
    {
        return $this->proteinGrams;
    }

    public function getFatGrams(): int
    {
        return $this->fatGrams;
    }

    public function getCarbsGrams(): int
    {
        return $this->carbsGrams;
    }
}
