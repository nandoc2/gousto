<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe\Dto;

use Gousto\Core\Recipe\ValueObject\DietType;

class RecipeCuisine
{
    /** @var DietType */
    private $dietType;

    /** @var string */
    private $season;

    /** @var string|null */
    private $base;

    /** @var string */
    private $proteinSource;

    /** @var int */
    private $preparationTimeMinutes;

    /** @var int */
    private $shelfLifeDays;

    /** @var string */
    private $equipmentNeeded;

    /** @var string */
    private $originCountry;

    /** @var string */
    private $recipeCuisine;

    /** @var string|null */
    private $inYourBox;

    public function __construct(
        DietType $dietType,
        string $season,
        ?string $base,
        string $proteinSource,
        int $preparationTimeMinutes,
        int $shelfLifeDays,
        string $equipmentNeeded,
        string $originCountry,
        string $recipeCuisine,
        ?string $inYourBox
    ) {
        $this->dietType = $dietType;
        $this->season = $season;
        $this->base = $base;
        $this->proteinSource = $proteinSource;
        $this->preparationTimeMinutes = $preparationTimeMinutes;
        $this->shelfLifeDays = $shelfLifeDays;
        $this->equipmentNeeded = $equipmentNeeded;
        $this->originCountry = $originCountry;
        $this->recipeCuisine = $recipeCuisine;
        $this->inYourBox = $inYourBox;
    }

    public function getDietType(): DietType
    {
        return $this->dietType;
    }

    public function getSeason(): string
    {
        return $this->season;
    }

    public function getBase(): ?string
    {
        return $this->base;
    }

    public function getProteinSource(): string
    {
        return $this->proteinSource;
    }

    public function getPreparationTimeMinutes(): int
    {
        return $this->preparationTimeMinutes;
    }

    public function getShelfLifeDays(): int
    {
        return $this->shelfLifeDays;
    }

    public function getEquipmentNeeded(): string
    {
        return $this->equipmentNeeded;
    }

    public function getOriginCountry(): string
    {
        return $this->originCountry;
    }

    public function getRecipeCuisine(): string
    {
        return $this->recipeCuisine;
    }

    public function getInYourBox(): ?string
    {
        return $this->inYourBox;
    }
}
