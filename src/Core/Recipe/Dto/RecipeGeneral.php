<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe\Dto;

use Gousto\Core\Recipe\ValueObject\BoxType;

class RecipeGeneral
{
    /** @var BoxType */
    private $boxType;

    /** @var string */
    private $title;

    /** @var string|null */
    private $shortTitle;

    /** @var string */
    private $slug;

    /** @var string */
    private $marketingDescription;

    /** @var string|null */
    private $bulletPoint1;

    /** @var string|null */
    private $bulletPoint2;

    /** @var string|null */
    private $bulletPoint3;

    public function __construct(
        BoxType $boxType,
        string $title,
        ?string $shortTitle,
        string $slug,
        string $marketingDescription,
        ?string $bulletPoint1,
        ?string $bulletPoint2,
        ?string $bulletPoint3
    ) {
        $this->boxType = $boxType;
        $this->title = $title;
        $this->shortTitle = $shortTitle;
        $this->slug = $slug;
        $this->marketingDescription = $marketingDescription;
        $this->bulletPoint1 = $bulletPoint1;
        $this->bulletPoint2 = $bulletPoint2;
        $this->bulletPoint3 = $bulletPoint3;
    }

    public function getBoxType(): BoxType
    {
        return $this->boxType;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getShortTitle(): ?string
    {
        return $this->shortTitle;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getMarketingDescription(): string
    {
        return $this->marketingDescription;
    }

    public function getBulletPoint1(): ?string
    {
        return $this->bulletPoint1;
    }

    public function getBulletPoint2(): ?string
    {
        return $this->bulletPoint2;
    }

    public function getBulletPoint3(): ?string
    {
        return $this->bulletPoint3;
    }
}
