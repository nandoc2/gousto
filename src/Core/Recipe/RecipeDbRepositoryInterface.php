<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe;

use Gousto\Core\Pagination\RequestPagination;
use Gousto\Core\Pagination\ResponsePagination;
use Gousto\Core\Recipe\Exception\RecipeNotFoundException;

interface RecipeDbRepositoryInterface
{
    /**
     * @param int $id
     * @return Recipe
     * @throws RecipeNotFoundException
     */
    public function getById(int $id): Recipe;

    /**
     * @param RequestPagination $pagination
     * @param string $cuisine
     * @return ResponsePagination
     */
    public function findByCuisine(RequestPagination $pagination, string $cuisine): ResponsePagination;

    /**
     * @param Recipe $recipe
     * @return Recipe
     */
    public function updateRecipe(Recipe $recipe): Recipe;

    /**
     * @param Recipe $recipe
     * @return Recipe
     */
    public function persistRecipe(Recipe $recipe): Recipe;
}
