<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe\Exception;

use Exception;

class RecipeNotFoundException extends Exception
{
}
