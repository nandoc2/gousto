<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe\ValueObject;

class BoxType
{
    private const TYPES = [
        'gourmet',
        'vegetarian',
    ];

    /** @var string */
    private $value;

    public function __construct(string $boxType)
    {
        if (!in_array($boxType, self::TYPES, true)) {
            throw new \InvalidArgumentException("Invalid box type value: ${boxType}");
        }

        $this->value = $boxType;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
