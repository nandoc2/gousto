<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe\ValueObject;

class DietType
{
    private const TYPES = [
        'meat',
        'fish',
        'vegetarian',
    ];

    /** @var string */
    private $value;

    public function __construct(string $dietType)
    {
        if (!in_array($dietType, self::TYPES, true)) {
            throw new \InvalidArgumentException("Invalid diet type: ${dietType}");
        }

        $this->value = $dietType;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
