<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe;

use Gousto\Core\Pagination\RequestPagination;
use Gousto\Core\Pagination\ResponsePagination;

class RecipeService
{
    /** @var RecipeDbRepositoryInterface */
    private $dbRepository;

    public function __construct(RecipeDbRepositoryInterface $dbRepository)
    {
        $this->dbRepository = $dbRepository;
    }

    public function getById(int $id): Recipe
    {
        return $this->dbRepository->getById($id);
    }

    public function findByCuisine(RequestPagination $pagination, string $cuisine): ResponsePagination
    {
        return $this->dbRepository->findByCuisine($pagination, $cuisine);
    }

    public function updateRecipe(Recipe $recipe): Recipe
    {
        return $this->dbRepository->updateRecipe($recipe);
    }

    public function createRecipe(Recipe $recipe): Recipe
    {
        return $this->dbRepository->persistRecipe($recipe);
    }
}
