<?php
declare(strict_types=1);

namespace Gousto\Core\Recipe;

use DateTimeImmutable;
use Gousto\Core\Recipe\Dto\RecipeCuisine;
use Gousto\Core\Recipe\Dto\RecipeGeneral;
use Gousto\Core\Recipe\Dto\RecipeNutrition;

class Recipe
{
    /** @var int|null */
    private $id;

    /** @var int */
    private $goustoReference;

    /** @var DateTimeImmutable */
    private $createdAt;

    /** @var DateTimeImmutable */
    private $updatedAt;

    /** @var RecipeGeneral */
    private $general;

    /** @var RecipeNutrition */
    private $nutrition;

    /** @var RecipeCuisine */
    private $cuisine;

    public function __construct(
        ?int $id,
        int $goustoReference,
        DateTimeImmutable $createdAt,
        DateTimeImmutable $updatedAt,
        RecipeGeneral $general,
        RecipeNutrition $nutrition,
        RecipeCuisine $cuisine
    ) {
        $this->id = $id;
        $this->goustoReference = $goustoReference;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->general = $general;
        $this->nutrition = $nutrition;
        $this->cuisine = $cuisine;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGoustoReference(): int
    {
        return $this->goustoReference;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getGeneral(): RecipeGeneral
    {
        return $this->general;
    }

    public function getNutrition(): RecipeNutrition
    {
        return $this->nutrition;
    }

    public function getCuisine(): RecipeCuisine
    {
        return $this->cuisine;
    }
}
