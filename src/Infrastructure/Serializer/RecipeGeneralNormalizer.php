<?php
declare(strict_types=1);

namespace Gousto\Infrastructure\Serializer;

use Gousto\Core\Recipe\Dto\RecipeGeneral;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class RecipeGeneralNormalizer implements NormalizerInterface
{
    /**
     * @param RecipeGeneral $object
     * @param null $format
     * @param array $context
     * @return mixed
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'box_type' => $object->getBoxType()->getValue(),
            'title' => $object->getTitle(),
            'short_title' => $object->getShortTitle(),
            'slug' => $object->getSlug(),
            'marketing_description' => $object->getMarketingDescription(),
            'bullet_point_1' => $object->getBulletPoint1(),
            'bullet_point_2' => $object->getBulletPoint2(),
            'bullet_point_3' => $object->getBulletPoint3(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof RecipeGeneral;
    }
}
