<?php
declare(strict_types=1);

namespace Gousto\Infrastructure\Serializer;

use Gousto\Core\Recipe\Dto\RecipeNutrition;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class RecipeNutritionNormalizer implements NormalizerInterface
{
    /**
     * @param RecipeNutrition $object
     * @param null $format
     * @param array $context
     * @return mixed
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'calories_kcal' => $object->getCaloriesKcal(),
            'protein_grams' => $object->getProteinGrams(),
            'fat_grams' => $object->getFatGrams(),
            'carbs_grams' => $object->getFatGrams(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof RecipeNutrition;
    }
}
