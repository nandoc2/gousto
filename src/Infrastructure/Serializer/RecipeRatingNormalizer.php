<?php
declare(strict_types=1);

namespace Gousto\Infrastructure\Serializer;

use Gousto\Core\RecipeRating\RecipeRating;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class RecipeRatingNormalizer implements NormalizerInterface
{
    private $recipeNormalizer;

    public function __construct(RecipeNormalizer $recipeNormalizer)
    {
        $this->recipeNormalizer = $recipeNormalizer;
    }

    /**
     * @param RecipeRating $object
     * @param null $format
     * @param array $context
     * @return mixed
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'star_rating' => $object->getStarRating()->getValue(),
            'recipe' => $this->recipeNormalizer->normalize($object->getRecipe(), $format, $context),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof RecipeRating;
    }
}
