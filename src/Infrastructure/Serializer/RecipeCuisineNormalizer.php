<?php
declare(strict_types=1);

namespace Gousto\Infrastructure\Serializer;

use Gousto\Core\Recipe\Dto\RecipeCuisine;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class RecipeCuisineNormalizer implements NormalizerInterface
{
    /**
     * @param RecipeCuisine $object
     * @param null $format
     * @param array $context
     * @return mixed
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'diet_type' => $object->getDietType()->getValue(),
            'season' => $object->getSeason(),
            'base' => $object->getBase(),
            'protein_source' => $object->getProteinSource(),
            'preparation_time_minutes' => $object->getPreparationTimeMinutes(),
            'shelf_life_days' => $object->getShelfLifeDays(),
            'equipment_needed' => $object->getEquipmentNeeded(),
            'origin_country' => $object->getOriginCountry(),
            'recipe_cuisine' => $object->getRecipeCuisine(),
            'in_your_box' => $object->getInYourBox(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof RecipeCuisine;
    }
}
