<?php
declare(strict_types=1);

namespace Gousto\Infrastructure\Serializer;

use Gousto\Core\Recipe\Recipe;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class RecipeNormalizer implements NormalizerInterface
{
    /** @var RecipeGeneralNormalizer */
    private $generalNormalizer;

    /** @var RecipeNutritionNormalizer */
    private $nutritionNormalizer;

    /** @var RecipeCuisineNormalizer */
    private $cuisineNormalizer;

    public function __construct(
        RecipeGeneralNormalizer $generalNormalizer,
        RecipeNutritionNormalizer $nutritionNormalizer,
        RecipeCuisineNormalizer $cuisineNormalizer
    ) {
        $this->generalNormalizer= $generalNormalizer;
        $this->nutritionNormalizer = $nutritionNormalizer;
        $this->cuisineNormalizer = $cuisineNormalizer;
    }

    /**
     * @param Recipe $object
     * @param null $format
     * @param array $context
     * @return mixed
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [
            'id' => $object->getId(),
            'gousto_reference' => $object->getGoustoReference(),
            'created_at'=> $object->getCreatedAt()->format(\DateTime::ATOM),
            'updated_at' => $object->getUpdatedAt()->format(\DateTime::ATOM),
        ];

        if (in_array('general', $context, true)) {
            $data['general'] = $this->generalNormalizer->normalize($object->getGeneral(), $format, $context);
        }

        if (in_array('nutrition', $context, true)) {
            $data['nutrition'] = $this->nutritionNormalizer->normalize($object->getNutrition(), $format, $context);
        }

        if (in_array('cuisine', $context, true)) {
            $data['cuisine'] = $this->cuisineNormalizer->normalize($object->getCuisine(), $format, $context);
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Recipe;
    }
}
