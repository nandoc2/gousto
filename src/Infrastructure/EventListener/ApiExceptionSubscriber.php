<?php
declare(strict_types=1);

namespace Gousto\Infrastructure\EventListener;

use Gousto\Core\Recipe\Exception\RecipeNotFoundException;
use InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    private const UNKNOWN_HTTP_CODE = 500;
    private const UNKNOWN_ERROR_CODE = 99999;
    private const DEFAULT_ERROR_MESSAGE = 'Unknown error';

    private const MAP = [
        400 => [
            InvalidArgumentException::class => 10000
        ],
        404 => [
            RecipeNotFoundException::class => 10400
        ]
    ];

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $e = $event->getException();

        $responseData = [
            'error' => [
                'code' => $this->getErrorCode(get_class($e)),
                'message' => empty($e->getMessage()) ? self::DEFAULT_ERROR_MESSAGE : $e->getMessage(),
            ]
        ];

        $event->setResponse(
            new JsonResponse($responseData, $this->getHttpCode(get_class($e)))
        );
    }

    private function getErrorCode(string $className): int
    {
        return self::MAP[$this->getHttpCode($className)][$className] ?? self::UNKNOWN_ERROR_CODE;
    }

    private function getHttpCode(string $className): int
    {
        foreach (self::MAP as $httpCode => $exceptions) {
            if (array_key_exists($className, $exceptions)) {
                return $httpCode;
            }
        }
        return self::UNKNOWN_HTTP_CODE;
    }
}
