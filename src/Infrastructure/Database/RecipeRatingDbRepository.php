<?php
declare(strict_types=1);

namespace Gousto\Infrastructure\Database;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;
use Gousto\Core\RecipeRating\RecipeRating;
use Gousto\Core\RecipeRating\RecipeRatingDbRepositoryInterface;

class RecipeRatingDbRepository implements RecipeRatingDbRepositoryInterface
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(Registry $registry)
    {
        $this->em = $registry->getManager('recipe');
    }

    /**
     * @inheritdoc
     */
    public function persistRecipeRating(RecipeRating $recipeRating): RecipeRating
    {
        $this->em->persist($recipeRating);
        $this->em->flush();

        return $recipeRating;
    }
}
