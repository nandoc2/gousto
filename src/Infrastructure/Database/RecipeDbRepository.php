<?php
declare(strict_types=1);

namespace Gousto\Infrastructure\Database;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Gousto\Core\Pagination\RequestPagination;
use Gousto\Core\Pagination\ResponsePagination;
use Gousto\Core\Recipe\Exception\RecipeNotFoundException;
use Gousto\Core\Recipe\Recipe;
use Gousto\Core\Recipe\RecipeDbRepositoryInterface;

class RecipeDbRepository implements RecipeDbRepositoryInterface
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(Registry $registry)
    {
        $this->em = $registry->getManager('recipe');
    }

    /**
     * @inheritdoc
     */
    public function getById(int $id): Recipe
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('r')
            ->from(Recipe::class, 'r')
            ->where('r.id = :id')
            ->setParameter('id', $id);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            throw new RecipeNotFoundException("Recipe with id ${id} not found");
        }
    }

    /**
     * @inheritdoc
     */
    public function findByCuisine(RequestPagination $pagination, string $cuisine): ResponsePagination
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('r')
            ->from(Recipe::class, 'r')
            ->where('r.cuisine.recipeCuisine = :cuisine')
            ->setParameter('cuisine', $cuisine);

        return $this->executePaginatedResult($qb, $pagination);
    }

    /**
     * @inheritdoc
     */
    public function updateRecipe(Recipe $recipe): Recipe
    {
        $this->em->merge($recipe);
        $this->em->flush();

        return $recipe;
    }

    /**
     * @inheritdoc
     */
    public function persistRecipe(Recipe $recipe): Recipe
    {
        $this->em->persist($recipe);
        $this->em->flush();

        return $recipe;
    }

    private function executePaginatedResult(QueryBuilder $qb, RequestPagination $pagination): ResponsePagination
    {
        $qb->setFirstResult($pagination->calculateOffset())
            ->setMaxResults($pagination->getPageSize());

        $qbPaginator = new Paginator($qb);

        return new ResponsePagination(
            $pagination->getPageNumber(),
            $pagination->getPageSize(),
            count($qbPaginator),
            $qb->getQuery()->getResult()
        );
    }
}
