<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190312184036 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE recipe (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, gousto_reference INTEGER NOT NULL, created_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , updated_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , title VARCHAR(255) NOT NULL, short_title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) NOT NULL, marketing_description VARCHAR(255) NOT NULL, bullet_point_1 VARCHAR(255) DEFAULT NULL, bullet_point_2 VARCHAR(255) DEFAULT NULL, bullet_point_3 VARCHAR(255) DEFAULT NULL, box_type VARCHAR(255) NOT NULL, calories_kcal SMALLINT NOT NULL, protein_grams SMALLINT NOT NULL, fat_grams SMALLINT NOT NULL, carbs_grams SMALLINT NOT NULL, season VARCHAR(255) NOT NULL, base VARCHAR(255) DEFAULT NULL, protein_source VARCHAR(255) NOT NULL, preparation_time_minutes SMALLINT NOT NULL, shelf_life_days SMALLINT NOT NULL, equipment_needed VARCHAR(255) NOT NULL, origin_country VARCHAR(255) NOT NULL, recipe_cuisine VARCHAR(255) NOT NULL, in_your_box CLOB DEFAULT NULL, diet_type VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX recipe_cuisine_idx ON recipe (recipe_cuisine)');
        $this->addSql('CREATE TABLE recipe_rating (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, recipe_id INTEGER UNSIGNED NOT NULL, star_rating SMALLINT NOT NULL)');
        $this->addSql('CREATE INDEX IDX_5597380359D8A214 ON recipe_rating (recipe_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE recipe');
        $this->addSql('DROP TABLE recipe_rating');
    }
}
